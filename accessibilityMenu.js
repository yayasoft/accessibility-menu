﻿if (typeof jQuery === 'undefined') {
    throw new Error('Accessibility Menu\'s JavaScript requires jQuery')
}
+function ($) {
    'use strict';
    var accessibilityClasses = {
        'monochrome': 'ASTmonochrome',
        'blackwhite': 'ASTblackwhite',
        'whiteblack': 'ASTwhiteblack',
        'fontsizeinc': 'ASTfontsize',
        'fontsizedec': 'ASTfontsize',
        'readablefont': 'ASTreadable',
        'whitecursor': 'ASTwhitecursor',
        'blackcursor': 'ASTblackcursor',
        'magnifier': 'ASTmagnifier',
        'highlightLinks': 'ASThighlightLinks',
        'highlightHeaders': 'ASThighlightHeaders',
        'alttext': 'ASTalttext'
    }

    var ASTColorSchemeArr = ['monochrome', 'blackwhite', 'whiteblack'];

    var accessibilityCookiesList = {
        'monochrome': {
            'name': 'ASTColorScheme',
            'value': 'monochrome'
        },
        'blackwhite': {
            'name': 'ASTColorScheme',
            'value': 'blackwhite'
        },
        'whiteblack': {
            'name': 'ASTColorScheme',
            'value': 'whiteblack'
        },
        'readablefont': {
            'name': 'ASTReadableFont',
            'value': 'readablefont'
        },
        'whitecursor': {
            'name': 'ASTBigCursor',
            'value': 'whitecursor'
        },
        'blackcursor': {
            'name': 'ASTBigCursor',
            'value': 'blackcursor'
        },
        'magnifier': {
            'name': 'ASTMagnifier',
            'value': 'magnifier'
        },
        'highlightLinks': {
            'name': 'ASTHighlightLinks',
            'value': 'highlightLinks'
        },
        'highlightHeaders': {
            'name': 'ASTHighlightHeaders',
            'value': 'highlightHeaders'
        },
        'alttext': {
            'name': 'ASTAltText',
            'value': 'alttext'
        }
    }

    var cookiesData = [
        'ASTColorScheme',
        'ASTFontSize',
        'ASTReadableFont',
        'ASTBigCursor',
        'ASTAltText',
        'ASTHighlightLinks',
        'ASTHighlightHeaders',
        'ASTMagnifier'
    ];

    function Accessibility() {
        this.language = []; // Available language settings, indexed by language code
        this.language[""] = { // Default language settings
            accessibility: "Accessibility",
            monochrome: "Monochrome",
            darkContrast: "Dark contrast",
            brightContrast: "Bright contrast",

            increaseFont: "Increase Font",
            decreaseFont: "Decrease Font",
            legibleFont: "Legible font",

            largeWhiteMarker: "Large white marker",
            largeBlackMarker: "Large black marker",
            zoomIn: "Zoom in",

            highlightLinks: "Highlight links",
            highlightingTitles: "Highlighting titles",
            descriptionOfThePictures: "Description of the pictures",
            disableAccessibility: "Disable accessibility",
            accessibilityDeclaration: "Accessibility Declaration",
            isRTL: false, // True if right-to-left language, false if left-to-right,
            accessibilityDeclarationLink: ""
        };
        this._defaults = {};
        this.language.en = $.extend(true, {}, this.language[""]);
        this.language["en-US"] = $.extend(true, {}, this.language.en);
    }


    $.extend(Accessibility.prototype, {
        /* Override the default settings for all instances of the date picker.
	     * @param  settings  object - the new settings to use as defaults (anonymous object)
	     * @return the manager object
	     */
        setDefaults: function (settings) {
            accessibility_extendRemove(this._defaults, settings || {});
            init();
            return this;
        },
        /* Get a setting value, defaulting if necessary. */
        _get: function (name) {
            return this._defaults[name];
        },
        _initAccessibilityFromCookie: function () {
            initAccessibilityFromCookie();
        },
        _menuContentTemplate: function () {
            var menuContentTpl = '<div id="ASTWrap"><div id="ASTaltTextBox" class="ASTaltTextBox"></div><div id="ASTmenu" aria-hidden="true" class="ASTmenu"><div class="ASTmenuHeader">';
            menuContentTpl += '<h2>' + this._get("accessibility") + '</h2>';
            menuContentTpl += '<div class="closebtn ASTmenu-close-btn">&times;</div></div><div class="ASTmenuBody"><div class="ASTmenuRow" data-rowdopt="monochrome">';
            menuContentTpl += '<div class="ASTmenuRowBlock" role="button" data-indopt="monochrome" aria-pressed="false">';
            menuContentTpl += '<div class="access-icon monochrome"></div>';
            menuContentTpl += '<div class="access-title">' + this._get("monochrome") + '</div>';
            menuContentTpl += '</div></div><div class="ASTmenuRow" data-rowdopt="blackwhite"><div class="ASTmenuRowBlock" role="button" data-indopt="blackwhite" aria-pressed="false">';
            menuContentTpl += '<div class="access-icon blackwhite"></div>';
            menuContentTpl += '<div class="access-title">' + this._get("darkContrast") + '</div>';
            menuContentTpl += '</div></div><div class="ASTmenuRow" data-rowdopt="whiteblack"><div class="ASTmenuRowBlock" role="button" data-indopt="whiteblack" aria-pressed="false">';
            menuContentTpl += '<div class="access-icon whiteblack"></div>';
            menuContentTpl += '<div class="access-title">' + this._get("brightContrast") + '</div>';
            menuContentTpl += '</div></div><div class="ASTmenuRow" data-rowdopt="fontsizeinc"><div class="ASTmenuRowBlock" role="button" data-indopt="fontsizeinc">';
            menuContentTpl += '<div class="access-icon fontsizeinc"></div>';
            menuContentTpl += '<div class="access-title">' + this._get("increaseFont") + '</div>';
            menuContentTpl += '</div></div><div class="ASTmenuRow" data-rowdopt="fontsizedec"><div class="ASTmenuRowBlock" role="button" data-indopt="fontsizedec">';
            menuContentTpl += '<div class="access-icon fontsizedec"></div>';
            menuContentTpl += '<div class="access-title">' + this._get("decreaseFont") + '</div>';
            menuContentTpl += '</div></div><div class="ASTmenuRow" data-rowdopt="readablefont"><div class="ASTmenuRowBlock" role="button" data-indopt="readablefont" aria-pressed="false">';
            menuContentTpl += '<div class="access-icon readablefont"></div>';
            menuContentTpl += '<div class="access-title">' + this._get("legibleFont") + '</div>';
            menuContentTpl += '</div></div><div class="ASTmenuRow" data-rowdopt="whitecursor"><div class="ASTmenuRowBlock" role="button" data-indopt="whitecursor" aria-pressed="false">';
            menuContentTpl += '<div class="access-icon whitecursor"></div>';
            menuContentTpl += '<div class="access-title">' + this._get("largeWhiteMarker") + '</div>';
            menuContentTpl += '</div></div><div class="ASTmenuRow" data-rowdopt="blackcursor"><div class="ASTmenuRowBlock" role="button" data-indopt="blackcursor" aria-pressed="false">';
            menuContentTpl += '<div class="access-icon blackcursor"></div>';
            menuContentTpl += '<div class="access-title">' + this._get("largeBlackMarker") + '</div>';
            menuContentTpl += '</div></div><div class="ASTmenuRow" data-rowdopt="magnifier"><div class="ASTmenuRowBlock" role="button" data-indopt="magnifier" aria-pressed="false">';
            menuContentTpl += '<div class="access-icon magnifier"></div>';
            menuContentTpl += '<div class="access-title">' + this._get("zoomIn") + '</div>';
            menuContentTpl += '</div></div><div class="ASTmenuRow" data-rowdopt="highlightLinks"><div class="ASTmenuRowBlock" role="button" data-indopt="highlightLinks" aria-pressed="false">';
            menuContentTpl += '<div class="access-icon links"></div>';
            menuContentTpl += '<div class="access-title">' + this._get("highlightLinks") + '</div>';
            menuContentTpl += '</div></div><div class="ASTmenuRow" data-rowdopt="highlightHeaders"><div class="ASTmenuRowBlock" role="button" data-indopt="highlightHeaders" aria-pressed="false">';
            menuContentTpl += '<div class="access-icon headers"></div>';
            menuContentTpl += '<div class="access-title">' + this._get("highlightingTitles") + '</div>';
            menuContentTpl += '</div></div><div class="ASTmenuRow" data-rowdopt="alttext"><div class="ASTmenuRowBlock" role="button" data-indopt="alttext" aria-pressed="false">';
            menuContentTpl += '<div class="access-icon alttext"></div>';
            menuContentTpl += '<div class="access-title">' + this._get("descriptionOfThePictures") + '</div>';
            menuContentTpl += '</div></div><div class="ASTmenuRow"><div class="ASTmenuRowBlockExit" role="button">';
            menuContentTpl += '<div class="access-icon disableAccessibility"></div>';
            menuContentTpl += '<div class="access-title">' + this._get("disableAccessibility") + '</div>';
            menuContentTpl += '</div></div><div class="ASTmenuRow"><div class="ASTmenuRowBlockExit" role="button"><div class="access-icon"></div><div class="access-title">';
            menuContentTpl += '<div attr-link="' + this._get("accessibilityDeclarationLink") + '" class="access-title-link">';
            menuContentTpl += this._get("accessibilityDeclaration");
            menuContentTpl += '</div></div></div></div></div></div></div>';

            return menuContentTpl;
        }
    });

    /* jQuery extend now ignores nulls! */
    function accessibility_extendRemove(target, props) {
        $.extend(target, props);
        for (var name in props) {
            if (props[name] == null) {
                target[name] = props[name];
            }
        }
        return target;
    }

    $.accessibility = new Accessibility(); // singleton instance
    $.accessibility.initialized = false;
    $.accessibility.version = "1.0.1";

    function menuBtTemplate() {
        return "<button id='ASTmenu-btn'><div class='ASTmenu-block'></div></button>";
    }

    function init() {
        $("body").append(menuBtTemplate());
        $("#ASTmenu").attr("aria-hidden", "true");
        var menuContentTemplate = $.accessibility._menuContentTemplate();
        $("body").append(menuContentTemplate);
        initMethods();
        initAccessibilityFromCookie();
    }
    
    function initMethods() {
        $('.access-title-link').on('click', function () {
            event.stopPropagation();
            var attrLink = $(this).attr("attr-link");
            window.open(attrLink, '_self');
        });
        $(document).on('click', function (e) {
            $("#ASTmenu").attr("aria-hidden", "true");
        });

        $('#ASTmenu-btn').on("click", function (event) {
            event.stopPropagation();
            $("#ASTmenu").attr("aria-hidden", "false");
        });

        $('#ASTmenu').on('click', function (event) {
            event.stopPropagation();
        });

        $('.ASTmenu-close-btn').on('click', function (event) {
            event.stopPropagation();
            $("#ASTmenu").attr("aria-hidden", "true");
        });

        $('.ASTmenuRowBlockExit').on('click', function (event) {
            event.stopPropagation();
            $.each(cookiesData, function (index, value) {
                delete_cookie(value);
            });
            location.reload();
        });

        $('.ASTmenuRowBlock').on('click', function (event) {
            event.stopPropagation();
            var ariaPressed = $(this).attr("aria-pressed");
            var classNameKey = $(this).attr("data-indopt");
            var accessibilityClass = accessibilityClasses[classNameKey];

            if (ariaPressed == undefined) {
                incDecFontSize(classNameKey, accessibilityClass);
                return;
            }

            if (classNameKey == 'alttext') {
                if (ariaPressed === 'false') {
                    altTextBind();
                } else {
                    altTextUnbind();
                }
            }

            if ($.inArray(classNameKey, ASTColorSchemeArr) > -1 && ariaPressed === 'false') {
                colorSchemeCheck(ariaPressed, classNameKey);
            }

            bigCursorCheck(ariaPressed, classNameKey);

            if (ariaPressed === 'false') {
                $(this).attr("aria-pressed", "true");
                $('body').addClass(accessibilityClass);
                set_cookie(accessibilityCookiesList[classNameKey].name, accessibilityCookiesList[classNameKey].value)
            } else {
                $(this).attr("aria-pressed", "false");
                $('body').removeClass(accessibilityClass);
                delete_cookie(accessibilityCookiesList[classNameKey].name)
            }
        });
    }

    // color scheme
    // Check only one
    function colorSchemeCheck(ariaPressed, classNameKey) {
        var arr = $.grep(ASTColorSchemeArr, function (value) {
            return value != classNameKey;
        });
        $.each(arr, function (index, value) {
            $('.ASTmenuRowBlock[data-indopt="' + value + '"]').attr("aria-pressed", "false");
            $('body').removeClass(accessibilityClasses[value]);
        });
    }

    // whitecursor and blackcursor
    // Check only one
    function bigCursorCheck(ariaPressed, classNameKey) {
        if ((classNameKey == 'whitecursor' || classNameKey == 'blackcursor') && ariaPressed === 'true') {
            return;
        }
        if (classNameKey == 'whitecursor') {
            $('.ASTmenuRowBlock[data-indopt="blackcursor"]').attr("aria-pressed", "false");
            $('body').removeClass(accessibilityClasses['blackcursor']);
        }
        if (classNameKey == 'blackcursor') {
            $('.ASTmenuRowBlock[data-indopt="whitecursor"]').attr("aria-pressed", "false")
            $('body').removeClass(accessibilityClasses['whitecursor']);
        }
    }

    // alttext 
    // Bind functions for img
    function altTextBind() {
        $("img").not("#ASTWrap img, #ASTmenu-btn img").bind({
            mouseover: function () {
                $("#ASTaltTextBox").text($(this).attr("alt"));
            },
            mousemove: function (event) {
                $("#ASTaltTextBox").css({
                    display: "block",
                    top: event.clientY + 35,
                    left: event.clientX - 50
                });
            },
            mouseleave: function (event) {
                $("#ASTaltTextBox").css({
                    display: "none"
                });
            }
        });
    }
    // alttext 
    // Remove Bind functions for img
    function altTextUnbind() {
        $("img").not("#ASTWrap img, #ASTmenu-btn img").unbind("mouseover mousemove mouseleave");
    }

    // Font- Size
    // Increase and Decrease
    function incDecFontSize(classKey, accessClass) {
        if ((incDecFontSizeTypeRate == 1 && classKey == 'fontsizedec') || (incDecFontSizeTypeRate == 1.5 && classKey == 'fontsizeinc')) {
            return;
        }

        var ASTfontsizeClass = "";

        ASTfontsizeClass = accessClass + "" + incDecFontSizeTypeRate.toString().replace('.', '');
        $('body').removeClass(ASTfontsizeClass);

        incDecFontSizeTypeRatePrev = incDecFontSizeTypeRate;
        incDecFontSizeTypeRate = classKey == 'fontsizedec' ? parseFloat(parseFloat(incDecFontSizeTypeRate - 0.1).toFixed(1)) : parseFloat(parseFloat(incDecFontSizeTypeRate + 0.1).toFixed(1));

        if (incDecFontSizeTypeRate > 1) {
            ASTfontsizeClass = accessClass + "" + incDecFontSizeTypeRate.toString().replace('.', '');
            $('body').addClass(ASTfontsizeClass);
        }

        incDecFontSizeTypeRate > 1 ? set_cookie("ASTFontSize", incDecFontSizeTypeRate) : delete_cookie("ASTFontSize");

        setFontSise(incDecFontSizeTypeRate, incDecFontSizeTypeRatePrev, 0);
    }

    function setFontSise(rate_new, rate_prev, delay) {
        setTimeout(function () {
            var thisFontSize = 0;
            $('body table, body thead, body tbody, body tr, body td, body span, body a, body p, body h1, body h2, body h3, body h4, body h5, body h6, body input, body label, body i').not('#ASTWrap, #ASTWrap *, a span, body span > span, body span > span > span')
                .each(function (index) {
                    thisFontSize = parseFloat($(this).css('font-size'));
                    thisFontSize = (thisFontSize / rate_prev) * rate_new;
                    $(this).css('font-size', thisFontSize);
                });
        }, delay);
    }

    function set_cookie(name, value, path, domain, secure) {
        var cookie_string = name + "=" + escape(value);

        var today = new Date();
        var expires = new Date();
        expires.setDate(today.getDate() + 1);
        cookie_string += "; expires=" + expires.toGMTString();

        cookie_string += "; path=" + escape("/");

        if (domain)
            cookie_string += "; domain=" + escape(domain);

        if (secure)
            cookie_string += "; secure";

        document.cookie = cookie_string;
    }

    function delete_cookie(cookie_name) {
        var cookie_date = new Date();
        cookie_date.setDate(cookie_date.getDate() - 3);
        document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString() + "; path=/";
    }

    function get_cookie(cookie_name) {
        var results = document.cookie.match('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');

        if (results)
            return (unescape(results[2]));
        else
            return null;
    }

    var incDecFontSizeTypeRate = 1;
    var incDecFontSizeTypeRatePrev = 1;
    function initAccessibilityFromCookie() {
        var cookieValue = '';
        var className = '';
        $.each(cookiesData, function (index, value) {
            cookieValue = get_cookie(value);
            if (cookieValue != undefined) {
                className = value != 'ASTFontSize' ? accessibilityClasses[cookieValue] : 'ASTfontsize' + cookieValue.replace('.', '');
                $('body').addClass(className);
                value != 'ASTFontSize' ? $('.ASTmenuRowBlock[data-indopt="' + cookieValue + '"]').attr("aria-pressed", "true") : null;

                if (cookieValue == 'alttext') {
                    setTimeout(function () {
                        altTextBind();
                    }, 2000);
                    return;
                }
                if (value == 'ASTFontSize') {
                    incDecFontSizeTypeRate = parseFloat(cookieValue);
                    setFontSise(incDecFontSizeTypeRate, 1, 2000);
                    return;
                }
            }
        });
    }
}(jQuery);
﻿define("accessibilityMenuHe", ["jquery"], function ($) {
    $.accessibility.language['he'] = {
        accessibility: "נגישוּת",
        monochrome: "מונוכרום",
        darkContrast: "ניגודיות כהה",
        brightContrast: "ניגודיות בהירה",
        increaseFont: "הגדלת גופן",
        decreaseFont: "הקטנת גופן",
        legibleFont: "גופן קריא",
        largeWhiteMarker: "סמן גדול לבן",
        largeBlackMarker: "סמן גדול שחור",
        zoomIn: "הגדלת תצוגה",
        highlightLinks: "הדגשת קישורים",
        highlightingTitles: "הדגשת כותרות",
        descriptionOfThePictures: "תיאור לתמונות",
        disableAccessibility: "בטל נגישות",
        accessibilityDeclaration: "הצהרת נגישות",
        isRTL: true, // True if right-to-left language, false if left-to-right,
        accessibilityDeclarationLink: "/accessibility-declaration"
    };

    return $.accessibility.language['he'];
});
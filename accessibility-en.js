﻿define("accessibilityMenuEn", ["jquery"], function ($) {
    $.accessibility.language['en'] = {
        accessibility: "Accessibility",
        monochrome: "Monochrome",
        darkContrast: "Dark contrast",
        brightContrast: "Bright contrast",
        increaseFont: "Increase Font",
        decreaseFont: "Decrease Font",
        legibleFont: "Legible font",
        largeWhiteMarker: "Large white marker",
        largeBlackMarker: "Large black marker",
        zoomIn: "Zoom in",
        highlightLinks: "Highlight links",
        highlightingTitles: "Highlighting titles",
        descriptionOfThePictures: "Description of the pictures",
        disableAccessibility: "Disable accessibility",
        accessibilityDeclaration: "Accessibility Declaration",
        isRTL: false, // True if right-to-left language, false if left-to-right,
        accessibilityDeclarationLink: "/accessibility-declaration"
    };

    return $.accessibility.language['en'];
});

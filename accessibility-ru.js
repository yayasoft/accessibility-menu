﻿define("accessibilityMenuRu", ["jquery"], function ($) {
    $.accessibility.language['ru'] = {
        accessibility: "Доступность",
        monochrome: "Монохромный",
        darkContrast: "Темный контраст",
        brightContrast: "Светлый контраст",
        increaseFont: "Увеличить шрифт",
        decreaseFont: "Уменьшить шрифт",
        legibleFont: "Разборчивый шрифт",
        largeWhiteMarker: "Большой белый курсор",
        largeBlackMarker: "Большой черный курсор",
        zoomIn: "Увеличить",
        highlightLinks: "Выделить ссылки",
        highlightingTitles: "Выделить заголовки",
        descriptionOfThePictures: "Описание фотографий",
        disableAccessibility: "Отключить доступность",
        accessibilityDeclaration: "Декларация доступности",
        isRTL: false, // True if right-to-left language, false if left-to-right,
        accessibilityDeclarationLink: "/accessibility-declaration"
    };

    return $.accessibility.language['ru'];
});